# coding:utf-8
from os import getenv
from app import app

app.debug = False

if __name__ == "__main__":
    app.run(host=getenv('HOST', '0.0.0.0'), port=int(getenv('PORT', '5000')))
