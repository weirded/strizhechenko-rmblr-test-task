# coding: utf-8

from unittest import TestCase, main
from controllers import RandomStringPool


class TestRandomStringPool(TestCase):

    def setUp(self):
        self.pool = RandomStringPool()

    def test_string_len(self):
        """
        можно передать символьную строку длиной не более M (M < 65536) символов
        """

        self.assertRaises(ValueError, self.pool.upload, 'A' * 65537)
        self.assertRaises(ValueError, self.pool.upload, 'A' * 65536)
        self.assertIs(self.pool.upload('A' * 65535), None)

    def test_get_single(self):
        """
        выдать по запросу список из строк,
        которые были загружены в сервис методом upload()
        """

        self.pool.get(0)
        self.pool.upload('TestString')
        self.assertEqual(self.pool.get(1), ['TestString'])

    def test_get_multiple(self):
        """
        возвращаем только то, что до этого сохранили
        """
        strings = map(chr, xrange(ord('a'), ord('z')))
        for item in strings:
            self.pool.upload(item)
        self.assertEqual(set(self.pool.get(26)), set(strings))

    def test_get_count(self):
        """
        выдать по запросу список из N < 100 строк,
        которые были загружены в сервис методом upload()
        """

        self.pool.get(0)

        for item in xrange(10):
            self.pool.upload(str(item))
        self.assertEqual(len(self.pool.get(10)), 10)

        for item in xrange(200):
            self.pool.upload(str(item))
        self.assertEqual(len(self.pool.get(200)), 99)

    def test_upload_duplicates(self):
        """
        склеивать дубликаты не нужно
        """

        self.pool.upload('ehal')
        self.pool.upload('ehal')
        self.pool.upload('ehal')
        self.assertEqual(self.pool.get(3), ['ehal'])

        self.pool.upload('ehal')
        self.pool.upload('ehal')
        self.pool.upload('greka')
        self.assertEqual(set(self.pool.get(2)), set(['ehal', 'greka']))

    def test_ranges(self):
        """
        "честный" random достигается преобразовыванием чисел "загрузок" строк
        в диапазоны, попадение случайного числа в которые определяет их выбор
        """
        self.pool.storage = {
            'greka': 1,
            'rode':  10,
            'over':  100,
            'river': 1000,
        }
        self.assertEqual(self.pool.ranges(), {
            'over':  (0, 99),
            'river': (100, 1099),
            'rode':  (1100, 1109),
            'greka': (1110, 1110),
        })

if __name__ == '__main__':
    main()
