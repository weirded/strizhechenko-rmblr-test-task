from sys import argv
from controllers import RandomStringPool

pool = RandomStringPool()
pool.storage = {
    'greka': 1,
    'rode':  10,
    'over':  100,
    'river': 1000,
}

if __name__ == '__main__':
    n = 3
    if len(argv) > 1:
        n = int(argv[1])
    print pool.storage
    print pool.ranges()
    print pool.get(n)
