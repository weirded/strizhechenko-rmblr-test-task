from random import randint


class RandomStringPool(object):

    def __init__(self):
        self.storage = {}
        self.max_string_size = 65535

    def ranges(self):
        num, output = 0, {}
        for key in self.storage:
            output[key] = (num, num + self.storage[key] - 1)
            num += self.storage[key]
        return output

    def get_random(self):
        r = randint(0, sum(self.storage.values()) - 1)
        for k, v in self.ranges().items():
            if v[0] <= r <= v[1]:
                del self.storage[k]
                return k

    def get_randoms(self, count):
        for _ in xrange(count):
            if not self.storage:
                break
            yield self.get_random()
        self.storage.clear()

    def get(self, count):
        count = min(count, 99)
        return list(self.get_randoms(count))

    def upload(self, string):
        if not isinstance(string, str) and not isinstance(string, unicode):
            raise ValueError, "Wrong input type: {}".format(type(string))
        if len(string) > self.max_string_size:
            raise ValueError, "Too long string: {}".format(len(string))
        self.storage[string] = self.storage.get(string, 0) + 1
