from flask import request, Response
from flask_api import FlaskAPI, status, exceptions
from random_string_pool.controllers import RandomStringPool

app = FlaskAPI(__name__)
pool = RandomStringPool()


@app.route("/get/<count>")
def get(count):
    return pool.get(int(count))


@app.route("/upload/<string>")
def upload(string):
    pool.upload(string)
    return ""

if __name__ == "__main__":
    app.run()
