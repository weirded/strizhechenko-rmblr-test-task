test:
	pytest app/random_string_pool/test.py
clean:
	find . -type f -name "*.pyc" -delete
	find . -type f -name "*.json" -delete
	find . -type f -name "*.pstats" -delete
	find . -type f -name "*.svg" -delete
env:
	virtualenv env
	sh -c ". env/bin/activate; pip install -r requirements.txt"
heroku:
	# You need to run following commands:
	#
	# heroku create APPNAME
	# heroku ps:scale web=1
