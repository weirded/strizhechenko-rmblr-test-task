# Сборка и запуск

## Запуск в Docker

```
git clone https://bitbucket.org/weirded/strizhechenko-rmblr-test-task
cd strizhechenko-rmblr-test-task
sudo docker build -t strizhechenko-rmblr-test-task .
sudo docker run -d -p 5000:5000 --name strizhechenko-rmblr-test-task strizhechenko-rmblr-test-task
# и когда надоест
sudo docker stop strizhechenko-rmblr-test-task
```

## Запуск на Linux/OS X

### Сборка/установка зависимостей

```
git clone https://bitbucket.org/weirded/strizhechenko-rmblr-test-task
cd strizhechenko-rmblr-test-task
make env
```

### Запуск

```
. env/bin/activate
HOST=0.0.0.0 PORT=5000 python run.py
```

# Примеры использования

В качестве примера, буду использовать развернутый на бесплатном Heroku экземпляр (маловероятно, но может уйти в спячку).

## Загрузка в сервис данных

Пустой вывод, для передачи информации об ошибках есть HTTP-коды.

```
curl -sSL http://strizhechenko-rmblr-test-task.herokuapp.com/upload/hello
curl -sSL http://strizhechenko-rmblr-test-task.herokuapp.com/upload/world
curl -sSL http://strizhechenko-rmblr-test-task.herokuapp.com/upload/hello
curl -sSL http://strizhechenko-rmblr-test-task.herokuapp.com/upload/rambler
```

## Получение результатов

```
curl -sSL http://strizhechenko-rmblr-test-task.herokuapp.com/get/2
```

Вывод в формате JSON. При использовании человеческих браузеров - всё показывается с "красивостями", при использовании `curl` и других утилит - ничего лишнего.

```
["hello", "world"]
```

# "Design notes"

Максимально простое решение, о производительности/отказоустойчивости/распределённости/работе после рестарта я не задумывался, так как для того, чтобы правильно их обеспечить нужно было больше данных, а вечером пятницы и в выходные выяснять как-то неловко.

Python - выбран ради лёгкости чтения кода.

Ещё немного интересно, для чего подобный реальный сервис использовался - в голову приходит выдача "лута" в какой-нибудь онлайн игре.
